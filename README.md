## EP1 ##
Projeto desenvolvido por Leonardo Medeiros (170038891) para a disciplina de Orientação a Objetos, Turma 'A' - 2018.1.

**Informações do software:**
	Abre, atravez de endereçamento inserido pelo usuario, uma imagem de formato 'ppm' ou 'pgm', e busca descobrir uma mensagem criptografada em seus pixels.


**Instruções para uso:**
1. Compilação:
	- Para compilar o codigo, basta usar o comando 'make' (referente ao arquivo Makefile).
	
1. Uso:
	- Para abrir o software, basta usar o comando 'make run' (referete ao arquivo Makefile).
	    - Após executado, o software descreverá instruções sobre como encaminhar a imagem.
