#include "ppm.hpp"

#include<vector>
#include<fstream>
#include<iostream>

using namespace std;

Ppm::Ppm(){
	chave = "";
}

Ppm::~Ppm(){}


void Ppm::setChave(string chave){
	this -> chave = chave;
}

void Ppm::lerImagem(){
	ifstream entrada;
	entrada.open(getCaminho()); // Abro novamente o arquivo
	char p;
	int tipo;
	entrada >> p >> tipo; // Necessario ler novamente
	char hastag;
	int inicio_criptografado;
	int tamanho_criptografado;
	string chave;
	int largura, altura, maximo;
	char rtemp,gtemp,btemp;
	
	entrada >> hastag >> inicio_criptografado >> tamanho_criptografado >> chave;
	entrada >> largura >> altura >> maximo;
	
	this -> chave = chave;
	
	setInicioCriptografado(inicio_criptografado);
	setTamanhoCriptografado(tamanho_criptografado);
	setLargura(largura);
	setAltura(altura);
	setMaximo(maximo);
	
	int count=0;
	int criptoCount=0;
	entrada.get();  // Elimina a quebra de linha depois do valor maximo
	for(int i=0; i<altura; i++){
		for(int j=0; j<largura; j++){
			entrada.get(rtemp);
			r.push_back(rtemp);
			if(count >= inicio_criptografado && criptoCount < (3*tamanho_criptografado)){
				criptografado.push_back(rtemp);
				criptoCount++;
			}
			count++;
			
			entrada.get(gtemp);
			r.push_back(gtemp);
			if(count >= inicio_criptografado && criptoCount < (3*tamanho_criptografado)){
				criptografado.push_back(gtemp);
				criptoCount++;
			}
			count++;
			
			entrada.get(btemp);
			r.push_back(btemp);
			if(count >= inicio_criptografado && criptoCount < (3*tamanho_criptografado)){
				criptografado.push_back(btemp);
				criptoCount++;
			}
			count++;
		}
	}
	
	entrada.close();
}

void Ppm::descobreMensagem(){
	/*
	cout << "Mensagem criptografada:" ;  // Uncomment para ver a mensagem criptografada
	
	for(auto it: criptografado){
		cout << it << " ";
	}
	cout << endl;
	*/
	
	//cout << "Nossa chave eh: " << chave << endl;
	
	vector <char> alfabeto={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	char novo_alfabeto[26];
	for(int k=0; k<26; k++){
		if(k<(int)chave.size()){
			novo_alfabeto[k]=chave[k];
			for(int i=0; i<26; i++){
				if(chave[k]==alfabeto[i]) {
					alfabeto[i] = '0';
				}
			}
		}else{
			for(int i=0; i<26; i++){
				if(alfabeto[i]!='0') {
					novo_alfabeto[k] = alfabeto[i];
					alfabeto[i]='0';
					break;
				}
			}
		}
	}
	/*
	for(int i=0; i<26; i++){  // Uncomment para ver todo o novo alfabeto
		cout << novo_alfabeto[i] << " ";
	}
	cout << endl;
	*/
	
	cout << "Mensagem descriptografada: " << endl;
	vector <char> alfabetoAux={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	for(int i=0; i<(int)criptografado.size(); i+=3){
		int pos, aux;
		
		pos=(criptografado[i]%10+criptografado[i+1]%10+criptografado[i+2]%10);
		//cout << "novo_alfabeto[" << pos << "] = " << novo_alfabeto[pos] << endl	;
		
		if(pos == 0){
			cout << " " ;
			continue;
		}else{
			for(int k=0; k<26; k++){
				if(alfabetoAux[pos-1] == novo_alfabeto[k]) {
					aux=k;
				}
			}
			cout << alfabetoAux[aux];
		}
		
	}
	cout << endl;
	
}
