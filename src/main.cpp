#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "imagem.hpp"	
#include "pgm.hpp"
#include "ppm.hpp"

using namespace std;

int main (){
	char temp;
	int tipo;
	string caminho;
	
	cout << "Insira o caminho para a imagem" << endl;
	cout << "(Exemplo: /home/usuario/Documents/imagem.ppm)" << endl;
	cin >> caminho;
	
	ifstream arq;
	
	/*   
	try{     // Nao funciona pois o metodo .open() eh 'exception safety'
		arq.open(caminho);
	} catch(...){
		cout << "ERRO: Arquivo nao encontrado, reinice o programa e insira um caminho valido" << endl;
	}
	*/
	
	arq.open(caminho);
	if(!(arq.good())) { // Verifica se a tentativa de abertura da imagem obteve sucesso
		cout << "ERRO: Arquivo nao encontrado, reinice o programa e insira um caminho valido" << endl; 
		return 0;
	}
	arq >> temp >> tipo;
	
	arq.close();
	switch(tipo){
		case 5:{
			cout << "A imagem inserida pertence ao formato PGM, com entrada padrao ASCII." << endl << endl;
			Pgm img;
			img.setCaminho(caminho);
			img.lerImagem();
			img.descobreMensagem();
			break;
		}
		case 6:{
			cout << "A imagem inserida pertence ao formato PPM, com entrada padrao ASCII." << endl << endl;
			Ppm img;
			img.setCaminho(caminho);
			img.lerImagem();
			img.descobreMensagem();
			break;
		}
		default:{
			cout << "ERRO: Formato de imagem nao suportado." << endl;
			break;
		}
	}
		
	return 0;
}
