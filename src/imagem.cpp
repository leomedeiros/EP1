#include "imagem.hpp"
#include <string>

Imagem::Imagem(){
	caminho = "";
	largura = 0;
	altura = 0;
	maximo = 0;
	inicio_criptografado = 0;
	tamanho_criptografado = 0;
}

Imagem::~Imagem(){}

void Imagem::setCaminho(string caminho){
	this -> caminho = caminho;
}

string Imagem::getCaminho(){
	return caminho;
}

void Imagem::setLargura(int largura){
	this -> largura = largura;
}

int Imagem::getLargura(){
	return largura;
}

void Imagem::setAltura(int altura){
	this -> altura = altura;
}

int Imagem::getAltura(){
	return altura;
}

void Imagem::setMaximo(int maximo){
	this -> maximo = maximo;
}

int Imagem::getMaximo(){
	return maximo;
}

void Imagem::setInicioCriptografado(int inicio_Criptografado){
	this -> inicio_criptografado = inicio_Criptografado;
}

void Imagem::setTamanhoCriptografado(int tamanho_criptografado){
	this -> tamanho_criptografado = tamanho_criptografado;
}
