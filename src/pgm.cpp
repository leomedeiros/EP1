#include "pgm.hpp"

#include<vector>
#include<fstream>
#include<iostream>

using namespace std;

Pgm::Pgm(){
	chave = 0;
}

Pgm::~Pgm(){}


void Pgm::setChave(int chave){
	chave = chave%26; //Reduz o valor da chave, sem alterar a criptografia
	this -> chave = chave;
}

void Pgm::lerImagem(){
	ifstream entrada;
	entrada.open(getCaminho()); // Abro novamente o arquivo
	char p;
	int tipo;
	entrada >> p >> tipo; // Necessario ler novamente
	char hastag;
	int inicio_criptografado;
	int tamanho_criptografado;
	int chave;
	int largura, altura, maximo;
	char ton;
	
	entrada >> hastag >> inicio_criptografado >> tamanho_criptografado >> chave;
	entrada >> largura >> altura >> maximo;
	
	this -> chave = chave;
	
	setInicioCriptografado(inicio_criptografado);
	setTamanhoCriptografado(tamanho_criptografado);
	setLargura(largura);
	setAltura(altura);
	setMaximo(maximo);
	
	int count=0;
	entrada.get(); // Elimina a quebra de linha depois do valor maximo
	for(int i=0; i<altura; i++){
		for(int j=0; j<largura; j++){
			entrada.get(ton);
			pixels.push_back(ton);
			if (count >= inicio_criptografado && count < inicio_criptografado+tamanho_criptografado){
				criptografado.push_back(ton);
			}
			count++;
		}
	}
	
	entrada.close();
}

void Pgm::descobreMensagem(){

	/*
	cout << "Mensagem criptografada: "; // Uncomment para ver a mensagem criptografada
	for(auto it: criptografado){
		cout << it;
	}
	cout << endl;
	*/
	
	//cout << "Nossa chave eh: " << chave << endl;
	
	vector <char> alfabeto={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	char novo_alfabeto[26];
	for(int k=0; k<26; k++){
		if(k+chave<26){
			novo_alfabeto[k]=alfabeto[k+chave];
		}else{
			novo_alfabeto[k]=alfabeto[k+chave-26];
		}
	}
	
	vector <char> ALFABETO={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char NOVO_ALFABETO[26];
	for(int k=0; k<26; k++){
		if(k+chave<26){
			NOVO_ALFABETO[k]=ALFABETO[k+chave];
		}else{
			NOVO_ALFABETO[k]=ALFABETO[k+chave-26];
		}
	}
	/* 
	for(int i=0; i<26; i++){  // Uncomment para ver todo o novo alfabeto
		cout << novo_alfabeto[i] << " ";
	}
	cout << endl;
	*/
	
	cout << "Mensagem descriptografada: ";
	
	for(auto it: criptografado){
		int pos;
		int flag=0;
		for(int i=0; i<26; i++){
			if(it == novo_alfabeto[i]){
				pos = i;
				flag = 1;
			}
		}
		for(int i=0; i<26; i++){
			if(it == NOVO_ALFABETO[i]){
				pos = i;
				flag = 2;
			}
		}
		if(flag == 1) cout << alfabeto[pos];
		else if (flag == 2) cout << ALFABETO[pos];
		else cout << it;
	}
	cout << endl;
}
