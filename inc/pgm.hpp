#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

#include <vector>

class Pgm: public Imagem{
	private:
		vector <unsigned char> pixels;
		int chave;
		vector <unsigned char> criptografado;
	public:
		Pgm();
		virtual ~Pgm();
		void setChave(int chave);
		virtual void lerImagem();
		virtual void descobreMensagem();
};

#endif // "PGM_HPP"
