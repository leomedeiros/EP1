#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

#include <vector>

class Ppm: public Imagem{
	private:
		vector <unsigned char> r;
		vector <unsigned char> g;
		vector <unsigned char> b;
		string chave;
		vector <unsigned char> criptografado;
	public:
		Ppm();
		virtual ~Ppm();
		void setChave(string chave);
		virtual void lerImagem();
		virtual void descobreMensagem();
};

#endif // "PPM_HPP"
