#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include<string>

using namespace std;

class Imagem{
	private:
		string caminho;
		int largura;
		int altura;
		int maximo;
		int inicio_criptografado;
		int tamanho_criptografado;
	public:
		Imagem();
		~Imagem();
		void setCaminho(string caminho);
		string getCaminho();
		void setLargura(int largura);
		int getLargura();
		void setAltura(int altura);
		int getAltura();
		void setMaximo(int maximo);
		int getMaximo();
		void setInicioCriptografado(int inicio_criptografado);
		void setTamanhoCriptografado(int tamanho_criptografado);
		virtual void lerImagem()=0;
		virtual void descobreMensagem()=0;

};

#endif // "IMAGEM_HPP"
